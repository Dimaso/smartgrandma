/**
 * Created by Alex on 06.06.2015.
 */

var io = require('socket.io').listen(3000);
var mongojs = require('mongojs');

var sockets = {};

console.log("listen port: 3000");

io.on('connection', function(socket) {
    console.log('connected');

    socket.on('ping', function () {
        console.log("receive ping");
        console.log("emit pong");
        socket.emit("pong");
    });

    socket.on('disconnect', function(){
        console.info("disconnect");
    });

    socket.on('message', function(msg){
        console.info(msg);
        if(socket.ticketNumber && sockets[socket.ticketNumber]){
            var recipientSocket = !socket.isGrandma ? sockets[socket.ticketNumber].grandma : recipientSocket = sockets[socket.ticketNumber].son;
            recipientSocket.emit("message", msg);
        }
    });    

    socket.emit("message", "Hey! Go to the gate number D2!");


    var pointId = 0;
    socket.emit("moveToPoint", pointId);

    socket.on("ticketNumber", function(ticketNumber, isGrandma){
        socket.ticketNumber = ticketNumber;
        socket.isGrandma = isGrandma;
        if(!sockets[ticketNumber]) {
            if(isGrandma){
                sockets[ticketNumber] = { grandma: socket }
            } else {
                sockets[ticketNumber] = { son: socket }
            }
        } else {
            if(isGrandma){
                sockets[ticketNumber].grandma = socket;
            } else {
                sockets[ticketNumber].son = socket;
            }
        }
    });
});