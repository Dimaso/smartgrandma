package com.example.smart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class InitActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    private Button travelerButton;
    private Button supporterButton;

    private View.OnClickListener travelerListener;
    private View.OnClickListener supporterListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeWindow();
        initializeApp();
    }

    private void initializeWindow() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.init_screen);
    }

    private void initializeApp() {
        travelerButton = (Button) findViewById(R.id.buttonTraveler);
        supporterButton = (Button) findViewById(R.id.buttonSupporter);

        travelerListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToTravelerActivity();
            }

        };
        supporterListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToSupporterActivity();
            }
        };
        travelerButton.setOnClickListener(travelerListener);
        supporterButton.setOnClickListener(supporterListener);
    }

    private void jumpToTravelerActivity() {
        Intent travelerIntent = new Intent(getApplicationContext(), RegistrationTravelerActivity.class);
        startActivity(travelerIntent);
    }

    private void jumpToSupporterActivity() {
        Intent supporterIntent = new Intent(getApplicationContext(), RegistrationSupporterActivity.class);
        startActivity(supporterIntent);
    }

}
