package com.example.smart;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Dmitriy on 06.06.2015.
 */
public class RegistrationSupporterActivity extends Activity {

    private Button scanQRButtonSupport;
    private Button supportButton;

    private View.OnClickListener scanQRListenerSupport;
    private View.OnClickListener supportListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeWindow();
        initializeApp();
    }

    private void initializeWindow() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.register_supporter);
    }

    private void initializeApp() {
        scanQRButtonSupport = (Button) findViewById(R.id.buttonScanBarCodeSupporter);
        supportButton = (Button) findViewById(R.id.buttonSupport);

        scanQRListenerSupport = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToScanQRActivity();
            }

        };
        supportListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToSupporterActivity();
            }
        };
        scanQRButtonSupport.setOnClickListener(scanQRListenerSupport);
        supportButton.setOnClickListener(supportListener);
    }

    private void jumpToScanQRActivity() {
        Toast.makeText(getApplicationContext(), "To be done", Toast.LENGTH_SHORT).show();
    }

    private void jumpToSupporterActivity() {
        Toast.makeText(getApplicationContext(), "To be done2", Toast.LENGTH_SHORT).show();

    }
}
