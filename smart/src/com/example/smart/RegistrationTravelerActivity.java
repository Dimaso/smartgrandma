package com.example.smart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.smart.fragments.TravelerActivity;

/**
 * Created by Dmitriy on 06.06.2015.
 */
public class RegistrationTravelerActivity extends Activity {

    private Button scanQRButton;
    private Button travelButton;

    private View.OnClickListener scanQRListener;
    private View.OnClickListener travelListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeWindow();
        initializeApp();
    }

    private void initializeWindow() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.register_traveler);
    }

    private void initializeApp() {
        scanQRButton = (Button) findViewById(R.id.buttonScanBarCodeTraveler);
        travelButton = (Button) findViewById(R.id.buttonTravel);

        scanQRListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToScanQRActivity();
            }

        };
        travelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToTravelerActivity();
            }
        };
        scanQRButton.setOnClickListener(scanQRListener);
        travelButton.setOnClickListener(travelListener);
    }

    private void jumpToScanQRActivity() {
        Toast.makeText(getApplicationContext(), "To be done", Toast.LENGTH_SHORT).show();
    }

    private void jumpToTravelerActivity() {
        TextView ticketNumberTextView = (TextView) findViewById(R.id.editTicketInfo);
        String ticketNumber = ticketNumberTextView.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString("ticketNumber", ticketNumber);
        Intent travelerMainIntent = new Intent(getApplicationContext(), TravelerActivity.class);
        travelerMainIntent.putExtras(bundle);
        startActivity(travelerMainIntent);
    }

}
