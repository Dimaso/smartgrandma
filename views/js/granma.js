
jQuery(document).ready(function($) {


    var $container = $('#img-pointer');
    var $item = $container.find('#container');

    /**
    -------------------------------
                son's app
    -------------------------------
    */

    /**
        map of spots of the image
    */
    var pointsArray = [
                        {top:235, left: 350},
                        {top:260, left: 360},
                        {top:280, left: 333},
                        {top:303, left: 308},
                        {top:300, left: 290},
                        {top:336, left: 290},
                        {top:412, left: 246},
                        {top:442, left: 227}
                        ];

    var currentStage = 0;
    var initialPosition = {top: 0, left: 0};

    /**
        Recursively moves point coordinates
        //todo uses global counters and pointsArray
    */
    function movePointOnMap(){
        
        if(!pointsArray[currentStage]) return;

        var calculatedPosition = { "top" : initialPosition.top + pointsArray[currentStage].left,
                                   "left": initialPosition.left + pointsArray[currentStage].top}

        $('.js-steps-list').find('li.collection-item').eq(currentStage).addClass('accomplished');

        currentStage++;
        var time = (Math.random()*5 + 1)*1000; //1..6 sec

        $item.animate(calculatedPosition, time,
                        function(){
                            movePointOnMap();
                        }); 
    }



    movePointOnMap();

    /**
    -------------------------------
                granma's app
    -------------------------------
    */

    function getTemplate (text, msgClass) {
      
        var timestamp = (new Date()).getHours() + ':' + (new Date()).getMinutes();
      
        return '<div class="msg ' + msgClass + '">' + 
               '<div class="time">' + timestamp + 
               '</div><div class="text">' + text + '</div></div>';
    
    };

    $('.container.msg-container').scrollTop(5000);

    /**
    Send message
    */
    function sendMsg(inputDOMel){
            var text = $(inputDOMel).val();

            $('.container.msg-container').append(getTemplate(text, 'msg-other'));
            event.preventDefault();
            socket.emit("message", text); 

            $(inputDOMel).val('');
            $('.container.msg-container').scrollTop(5000);
            updateMsgCount();
    }

    /**
    Adds message from correspondent
    */
     function putMsg(text){
            $('.container.msg-container').append(getTemplate(text, 'msg-my'));
            $('.container.msg-container').scrollTop(5000);
            updateMsgCount();
    }

    function updateMsgCount(){
        $('.js-messages-counter').text($('.container.msg-container .msg').length + ' messages');
    }
    /**
    Sending message by clicking 'Enter'
    */
    $(document).on('keyup', '#icon_prefix2', function(event){
        if(event.keyCode == 13 && $(this).val().length > 3){
            sendMsg($(this));
        }
    })

    /**
    Sending message by clicking <button>
    */
    $('#js-send-msg-trigger').click(function(event) {
        if($('#icon_prefix2').val().length > 3){
         sendMsg($('#icon_prefix2'));
        }
    });

    /**
    toggle chat on mobile
    */
    $('.js-collapse-chat').click(function(event) {
        /* Act on the event */
        $(this).find('i').toggleClass('mdi-navigation-expand-less mdi-navigation-expand-more');
        $('.container.msg-container, .input-field.col.s12').toggle();
    });




    /**
    ---------------------
        Socket setup
    ---------------------
    **/

    var socket = io(':3000');
    socket.on('connect', function(){
        console.log("connected");
        var ticketNumber = 422;

        if(ticketNumber)
            socket.emit("ticketNumber", ticketNumber, isGrandma);
    });

    socket.emit("ping", function(){
        console.log("emit ping");
        console.log(arguments);
    });
    socket.on("pong", function(){
        console.log("receive pong");
        console.log(arguments);
    });

    socket.on("moveToPoint", function(pointId){
        console.log(pointId);
    });

    socket.on("message", function(msg) {
        putMsg(msg);
    });

    socket.connect();
    
});

